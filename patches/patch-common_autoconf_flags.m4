$NetBSD$

--- common/autoconf/flags.m4.orig	2018-12-29 17:58:02.000000000 +0000
+++ common/autoconf/flags.m4
@@ -359,6 +359,11 @@ AC_DEFUN([FLAGS_SETUP_COMPILER_FLAGS_FOR
       SET_SHARED_LIBRARY_ORIGIN="$SET_EXECUTABLE_ORIGIN"
       SET_SHARED_LIBRARY_NAME='-Wl,-install_name,@rpath/[$]1'
       SET_SHARED_LIBRARY_MAPFILE='-Wl,-exported_symbols_list,[$]1'
+    elif test "x$OPENJDK_TARGET_OS" = xsolaris; then
+      SET_EXECUTABLE_ORIGIN='-Wl,-R,\$$ORIGIN[$]1'
+      SET_SHARED_LIBRARY_ORIGIN="-Wl,$SET_EXECUTABLE_ORIGIN"
+      SET_SHARED_LIBRARY_NAME='-Wl,-h,[$]1'
+      SET_SHARED_LIBRARY_MAPFILE='-Wl,-M,[$]1'
     else
       # Default works for linux, might work on other platforms as well.
       SHARED_LIBRARY_FLAGS='-shared'
