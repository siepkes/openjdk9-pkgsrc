$NetBSD$

--- hotspot/src/share/vm/gc/cms/compactibleFreeListSpace.cpp.orig	2018-12-29 18:01:47.000000000 +0000
+++ hotspot/src/share/vm/gc/cms/compactibleFreeListSpace.cpp
@@ -49,6 +49,9 @@
 //// CompactibleFreeListSpace
 /////////////////////////////////////////////////////////////////////////
 
+template <class Chunk_t, class FreeList_t>
+size_t TreeChunk<Chunk_t, FreeList_t>::_min_tree_chunk_size = sizeof(TreeChunk<Chunk_t,  FreeList_t>)/HeapWordSize;
+
 // highest ranked  free list lock rank
 int CompactibleFreeListSpace::_lockRank = Mutex::leaf + 3;
 
