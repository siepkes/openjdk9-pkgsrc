$NetBSD$

--- hotspot/src/share/vm/adlc/arena.cpp.orig	2018-12-29 18:01:37.000000000 +0000
+++ hotspot/src/share/vm/adlc/arena.cpp
@@ -24,6 +24,7 @@
 
 #include "adlc.hpp"
 
+
 void* Chunk::operator new(size_t requested_size, size_t length) throw() {
   return CHeapObj::operator new(requested_size + length);
 }
