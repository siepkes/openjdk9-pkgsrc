#!/bin/sh

hg clone http://hg.openjdk.java.net/jdk9/jdk9/ openjdk-jdk9
cd j openjdk-jdk9
hg checkout jdk-9+181    
sh get_source.sh 
find . -type d -name .hg -print -exec rm -rf {} \;
cd ..
tar czf /data/distfiles/openjdk-jdk9-1.9.181.tar.gz openjdk-jdk9
rm -rf openjdk-jdk9

