$NetBSD$

--- hotspot/src/share/vm/adlc/arena.hpp.orig	2018-12-29 18:01:37.000000000 +0000
+++ hotspot/src/share/vm/adlc/arena.hpp
@@ -72,6 +72,7 @@ class Chunk: public CHeapObj {
  public:
   void* operator new(size_t size, size_t length) throw();
   void  operator delete(void* p, size_t length);
+  void  operator delete(void* p);
   Chunk(size_t length);
 
   enum {
